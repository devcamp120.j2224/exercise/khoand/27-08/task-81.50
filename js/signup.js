$('#btn-sign-up').on('click', function () {
  onBtnSignUpClick();
});

function onBtnSignUpClick() {
  var vUserObj = {
    firstname: '',
    lastname: '',
    email: '',
    password: '',
  };
  vUserObj.firstname = $('#input-first-name').val().trim();
  vUserObj.lastname = $('#input-last-name').val().trim();
  vUserObj.email = $('#input-email').val().trim();
  vUserObj.password = $('#input-password').val().trim();
  var vIsChecked = validateData(vUserObj);
  if (vIsChecked) {
    callApiToSignUp(vUserObj);
  }
}

function validateData(pUserObj) {
  var vResult = true;
  try {
    if (pUserObj.firstname === '') {
      throw 'Vui long nhap first name';
      vResult = false;
    }
    if (pUserObj.lastname === '') {
      throw 'Vui long nhap last name';
      vResult = false;
    }
    if (pUserObj.email === '') {
      throw 'Vui long nhap email';
      vResult = false;
    }
    if (pUserObj.password === '') {
      throw 'Vui long nhap password';
      vResult = false;
    }
    if (pUserObj.password !== $('#input-confirm-password').val().trim()) {
      throw 'Password khong giong nhau';
      vResult = false;
    }
  } catch (error) {
    alert(error);
  }
  return vResult;
}

function callApiToSignUp(pUserObj) {
  $.ajax({
    url: 'http://42.115.221.44:8080/devcamp-auth/users/signup',
    type: 'POST',
    dataType: 'json',
    contentType: 'application/json; charset=utf-8',
    data: JSON.stringify(pUserObj),
    success: function (pRes) {
      console.log(pRes);
      alert(pRes.message);
    },
    error: function (pAjaxContext) {
      console.log(pAjaxContext.responseJSON.message);
    },
  });
}
