$(document).ready(function () {
  const token = getCookie('token');
  if (token) {
    window.location.href = './info.html';
  }
});
$('#btn-login').on('click', function () {
  onBtnLoginClick();
});

function onBtnLoginClick() {
  var vLoginData = {
    email: '',
    password: '',
  };
  vLoginData.email = $('#input-email').val().trim();
  vLoginData.password = $('#input-password').val().trim();
  var vIsChecked = validateData(vLoginData);
  if (vIsChecked) {
    callApiToLogin(vLoginData);
  }
}

function validateData(pUserObj) {
  var vResult = true;
  try {
    if (pUserObj.email === '') {
      throw 'Vui long nhap email';
      vResult = false;
    }
    if (pUserObj.password === '') {
      throw 'Vui long nhap password';
      vResult = false;
    }
  } catch (error) {
    alert(error);
  }
  return vResult;
}

function callApiToLogin(pLoginData) {
  $.ajax({
    url:
      'http://42.115.221.44:8080/devcamp-auth/users/signin?email=' +
      pLoginData.email +
      '&password=' +
      pLoginData.password,
    type: 'POST',
    dataType: 'json',
    contentType: 'application/json; charset=utf-8',
    success: function (pRes) {
      console.log(pRes);
      setCookie('token', pRes.token, 1);
      window.location.href = './info.html';
    },
    error: function (pAjaxContext) {
      console.log(pAjaxContext.responseText);
    },
  });
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = 'expires=' + d.toUTCString();
  document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
  console.log('cookie: ' + document.cookie);
}

function getCookie(cname) {
  var name = cname + '=';
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}
