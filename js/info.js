$(document).ready(function () {
  callApiToGetInfo();
});
const gToken = getCookie('token');
$('#btn-logout').on('click', function () {
  onBtnLogoutClick();
});

function onBtnLogoutClick() {
  redirectToLogin();
}

function callApiToGetInfo() {
  if (gToken == '') {
    alert('Chưa có token');
    window.location.href = './login.html';
  }

  $.ajax({
    url: 'http://42.115.221.44:8080/devcamp-auth/users/me',
    type: 'GET',
    dataType: 'json',
    contentType: 'application/json; charset=utf-8',
    headers: {
      Authorization: 'Bearer ' + gToken,
    },
    success: function (pRes) {
      console.log(pRes);
      loadDataToForm(pRes);
    },
    error: function (pAjaxContext) {
      console.log(pAjaxContext.responseText);
    },
  });
}
function loadDataToForm(pData) {
  $('#input-first-name').val(pData.firstname);
  $('#input-last-name').val(pData.lastname);
  $('#input-email').val(pData.email);
}
function redirectToLogin() {
  // Trước khi logout cần xóa token đã lưu trong cookie
  setCookie('token', '', 1);
  window.location.href = 'login.html';
}
function getCookie(cname) {
  var name = cname + '=';
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = 'expires=' + d.toUTCString();
  document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}
